Array.prototype.where = function (filter) {
    switch(typeof filter) {
        case 'function':
            return $.grep(that, filter);

        case 'object':
            var filtered = this;
            for(var prop in filter) {
                if(!filter.hasOwnProperty(prop)) {
                    continue; // ignore inherited properties
                }
                filtered = $.grep(filtered, function (item) {
                    return item[prop] === filter[prop];
                });
            }
            return filtered.slice(0); // copy the array
        // (in case of empty object filter)

        default:
            throw new TypeError('func must be either a' +
                'function or an object of properties and values to filter by');
    }
};

Array.prototype.firstOrDefault = function(func){
    return this.where(func)[0] || null;
};

Array.prototype.extend = function (other_array) {
    /* you should include a test to check whether other_array really is an array */
    other_array.forEach(function(v) {this.push(v)}, this);
}
