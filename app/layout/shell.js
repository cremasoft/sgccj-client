﻿(function () { 
    'use strict';
    
    var controllerId = 'shell';
    angular.module('app').controller(controllerId,
        ['$rootScope', '$location', 'common', 'config','authService', '$window', shell]);

    function shell($rootScope, $location, common, config, authService, $window) {
        var vm = this;
        var logSuccess = common.logger.getLogFn(controllerId, 'success');
        var events = config.events;
        vm.busyMessage = 'Please wait ...';
        vm.isBusy = true;
        vm.spinnerOptions = {
            radius: 40,
            lines: 7,
            length: 0,
            width: 30,
            speed: 1.7,
            corners: 1.0,
            trail: 100,
            color: '#F58A00'
        };
        vm.isAuth = false;
        vm.logout = logout;

        //activate();

        function activate() {
//            logSuccess('Main view loaded!', null, true);
            common.activateController([], controllerId);
        }

        function toggleSpinner(on) { vm.isBusy = on; }

        function openApp() {
            vm.isAuth = true;
            activate();
            $location.path('/applications');
        }

        function logout() {
            vm.isAuth = false;
            //authService.logout();
        }

        $rootScope.$on('$routeChangeStart',
            function (event, next, current) {
//                var answer = $window.confirm('Leave?');
//                if(!answer){
//                    event.preventDefault();
//                    return;
//                }
                toggleSpinner(true);
            }
        );
        
        $rootScope.$on(events.controllerActivateSuccess,
            function (data) { toggleSpinner(false); }
        );

        $rootScope.$on(events.spinnerToggle,
            function (data) { toggleSpinner(data.show); }
        );

        $rootScope.$on(events.authLoginSuccess,
            function (data) { openApp(); }
        );
    };
})();