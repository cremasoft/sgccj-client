(function () {
    'use strict';

    var serviceId = 'routeMediator';
    angular.module('app').factory(serviceId, ['$rootScope', '$location', 'config', routeMediator]);

    function routeMediator($rootScope, $location, config) {

        var handleRouteChangeError = false;

        var service = {
            updateDocTitle: _updateDocTitle
        };

        return service;

        function setRoutingHandlers(){
            _updateDocTitle();
            _handleRoutingErrors();
        }

        function _handleRoutingErrors() {
            $rootScope.$on('$routeChangeError',
                function(event, current, previous, rejection){
                    if (handleRouteChangeError) { return; }
                    handleRouteChangeError = true;
                    var msg = 'Error routing: ' + (current && current.name) + '. ' + (rejection.msg || '');
                    console.log(msg);
                    $location.path('/');
                });
        }

        function _updateDocTitle(){
            $rootScope.$on('$routeChangeSuccess',
                function(event, current, previous){
                    handleRouteChangeError = false;
                    var title = config.docTitle + ' ' + (current.title || '');
                    $rootScope.title = title;
                });
        }
    }
})();