﻿(function () {
    'use strict';

    var serviceId = 'authInterceptorService';
    angular.module('app').factory(serviceId, ['$q', '$location', 'localStorageService', authInterceptorService]);

    function authInterceptorService($q, $location, localStorageService) {

        var serviceBase = 'http://localhost:26264/';

        var service = {
            request: _request,
            responseError: _responseError
        };

        return service;

        var _request = function (config) {

            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        }

        var _responseError = function (rejection) {
            if (rejection.status === 401) {
                $location.path('/login');
            }
            return $q.reject(rejection);
        }
    }
})();