﻿(function () {
    'use strict';

    var serviceId = 'authService';
    angular.module('app').factory(serviceId, ['$http', '$q', 'localStorageService', 'common', 'commonConfig', authService]);

    function authService($http, $q, localStorageService, common, commonConfig) {

        var serviceBase = 'http://localhost:26264/';
        var _authentication = {
            isAuth: false,
            userName : ""
        };

        var service = {
            login: _login,
            logout: _logOut,
            fillAuthData: _fillAuthData,
            authentication: _authentication
        };

        return service;

        function _login(loginData) {
            var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

            var deferred = $q.defer();

            $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName });

                _authentication.isAuth = true;
                _authentication.userName = loginData.userName;

                deferred.resolve(response);

            }).error(function (err, status) {
                _logOut();
                deferred.reject(err);
            });

            return deferred.promise;

        }

        function _logOut() {

            localStorageService.remove('authorizationData');

            _authentication.isAuth = false;
            _authentication.userName = "";

        }

        function _fillAuthData() {

            // TODO: Check if the token has expired

            var authData = localStorageService.get('authorizationData');
            if (authData)
            {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
                common.$broadcast(commonConfig.config.authLoginSuccessEvent, {});
            }

        }
    }
})();