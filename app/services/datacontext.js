(function () {
    'use strict';

    var serviceId = 'datacontext';
    angular.module('app').factory(serviceId, ['common','config','$resource', '$location','$http', datacontext]);

    function datacontext(common, config, $resource, $location, $http) {

        var getLogFn = common.logger.getLogFn;

        var $q = common.$q;

        // Local data
        var cemeteries = [];
        var graves = [];
        var citizens = [];
        var deceased = [];
        var applications = [];
        var purchases = [];

        var service = {
            cemeteries: cemeteries,
            graves: graves,
            citizens: citizens,
            deceased: deceased,
            applications: applications,
            purchases: purchases,

            getEntityChanges: getEntityChanges,

            // Application
            getApplications: getApplications,
            getApplicationById: getApplicationById,
            addApplication: addApplication,
            editApplication: editApplication,

            // Purchase
            getPurchases: getPurchases,
            getPurchaseById: getPurchaseById,
            addPurchase: addPurchase,
            editPurchase: editPurchase,

            // Inhumation
            getDeceased: getDeceased,
            getDeceasedById: getDeceasedById,
            editDeceased: editDeceased,
            getBurials: getBurials,
            addBurial: addBurial,

            // Management
            getCemeteries: getCemeteries,
            getCemeteryById: getCemeteryById,
            addCemetery: addCemetery,
            editCemetery: editCemetery,
            deleteCemetery: deleteCemetery,
            getCitizens: getCitizens,
            getCitizenById: getCitizenById,
            addCitizen: addCitizen,
            editCitizen: editCitizen,
            getGraves: getGraves,
            getGraveById: getGraveById,
            deleteGrave: deleteGrave,
            editGrave: editGrave,
            addGrave: addGrave,
            uploadImage: uploadImage,
            addIntimation: addIntimation,

            // Registries
            getInhumationRegistry: getInhumationRegistry,
            getGraveRegistry: getGraveRegistry,
            getGraveMonumentsRegistry: getGraveMonumentsRegistry,
            getAbandonedDeceasedRegistry: getAbandonedDeceasedRegistry,
            getDeceasedRegistry: getDeceasedRegistry,
            getInhumationRequestRegistry: getInhumationRequestRegistry,
            getPurchaseContractRegistry: getPurchaseContractRegistry,
            getIntimationRegistry: getIntimationRegistry,

            // Export registries
            exportInhumationRegistry: exportInhumationRegistry,
            exportGraveRegistry: exportGraveRegistry,
            exportGraveMonumentsRegistry: exportGraveMonumentsRegistry,
            exportAbandonedDeceasedRegistry: exportAbandonedDeceasedRegistry,
            exportDeceasedRegistry: exportDeceasedRegistry,
            exportInhumationRequestRegistry: exportInhumationRequestRegistry,
            exportPurchaseContractRegistry: exportPurchaseContractRegistry,
            exportIntimationRegistry: exportIntimationRegistry
        };

        return service;

        // Start Applications

        function getApplications() {
            return $resource(config.remoteServiceName + "/Application", {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getApplicationById(applicationId) {
            var foundApplication = [];
            if (this.applications) {
                this.applications.forEach(function (application) {
                    if (application.infocetNumber == applicationId) {
                        foundApplication = application;
                    }
                });
            }
            return $q.when(foundApplication);
        }

        function addApplication(application) {
            return $http.post(config.remoteServiceName + "/application", application);
        }

        function editApplication(application) {
            return $http.put(config.remoteServiceName + "/application", application);
        }

        // End Applications

        // Start Purchases

        function getPurchases() {
            return $resource(config.remoteServiceName + "/purchase", {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getPurchaseById(purchaseId) {
            var foundPurchase = [];
            if (this.purchases) {
                this.purchases.forEach(function (purchase) {
                    if (purchase.contractNumber == purchaseId) {
                        foundPurchase = purchase;
                    }
                });
            }
            return $q.when(foundPurchase);
        }

        function addPurchase(purchase) {
            return $http.post(config.remoteServiceName + "/purchase", purchase);
        }

        function editPurchase(purchase) {
            return $http.put(config.remoteServiceName + "/purchase", purchase);
        }

        // End Purchases

        // Start Management

        function getCitizens() {
//            var deceased = [
//                {ssn: '1234566', firstName: 'FirstName', lastName: 'LastName', address: 'address' },
//                {ssn: '1234566', firstName: 'FirstName', lastName: 'LastName', address: 'address' },
//                {ssn: '1234566', firstName: 'FirstName', lastName: 'LastName', address: 'address' }
//            ];
//            return $q.when(deceased);
            return $resource(config.remoteServiceName + "/civilian", {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getCitizenById(citizenId) {
            var foundCitizen = [];
            if (this.citizens) {
                this.citizens.forEach(function (citizen) {
                    if (citizen.ssn == citizenId) {
                        foundCitizen = citizen;
                    }
                });
            }
            return $q.when(foundCitizen);
        }

        function addCitizen(citizen) {
            return $http.post(config.remoteServiceName + "/civilian", citizen);
        }

        function editCitizen(citizen) {
             return $http.put(config.remoteServiceName + "/civilian", citizen);
        }

        function getCemeteries() {
//            var cemeteries = [{
//                id: 1234,
//                name: 'Name3',
//                address: 'Adresa3',
//                plots: [
//                    {
//                        id: 1235,
//                        cemeteryId: 123,
//                        number: 2,
//                        nograves: 5
//                    }
//                ],
//                intimations: [
//                    {
//                        id: 1235,
//                        cemeteryId: 123,
//                        description: "blabla",
//                        date: '5 - 2 - 2012'
//                    }
//                ],
//                graves: [
//                    {
//                        id: 1234,
//                        number: 42222,
//                        cemeteryName: 'adfasdf',
//                        cemeteryId: 123,
//                        plotNumber: 1235,
//                        surface: 'suprafata',
//                        inhumations: [
//                            {
//                                nopermit: 1234,
//                                graveId: 1234,
//                                date: '1-2-2302',
//                                noreceipt: 1234,
//                                deceased: {
//                                    ssn: 13123,
//                                    firstName: 'firstname',
//                                    lastName: 'lastname',
//                                    religion: 'religion',
//                                    isAbandoned: 'abandoned'
//                                }
//                            }
//                        ],
//                        isMonument: false,
//                        additionalInfo: 'info',
//                        image: 'http://thomasgenweb.com/John_J_Thomas_grave.jpg'
//                    }
//                ]
//            },
//                {
//                    id: 1234,
//                    name: 'Name3',
//                    address: 'Adresa3',
//                    plots: [
//                        {
//                            id: 1235,
//                            cemeteryId: 123,
//                            number: 2,
//                            nograves: 5
//                        }
//                    ],
//                    intimations: [
//                        {
//                            id: 1235,
//                            cemeteryId: 123,
//                            description: "blabla",
//                            date: '5 - 2 - 2012'
//                        }
//                    ],
//
//            graves: [
//                {
//                    id: 1234,
//                    number: 12222,
//                    cemeteryName: 'adfasdf',
//                    cemeteryId: 123,
//                    plotNumber: 1235,
//                    surface: 'suprafata',
//                    inhumations: [
//                        {
//                            nopermit: 1234,
//                            graveId: 1234,
//                            date: '1-2-2302',
//                            noreceipt: 1234,
//                            deceased: {
//                                ssn: 13123,
//                                firstName: 'firstname',
//                                lastName: 'lastname',
//                                religion: 'religion',
//                                isAbandoned: 'abandoned'
//                            }
//                        }
//                    ],
//                    isMonument: false,
//                    additionalInfo: 'info',
//                    image: 'http://thomasgenweb.com/John_J_Thomas_grave.jpg'
//                }
//            ]
//        }];
//
//        this.cemeteries = cemeteries;
//        return $q.when(cemeteries);
        return $resource(config.remoteServiceName + "/cemeteries", {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
    }

    function getCemeteryById(cemeteryId) {
        var foundCemetery = [];
        if (this.cemeteries) {
            this.cemeteries.forEach(function (cemetery) {
                if (cemetery.id == cemeteryId) {
                    foundCemetery = cemetery;
                }
            });
        }
        return $q.when(foundCemetery);
    }

    function addCemetery(cemetery) {
        return $http.post(config.remoteServiceName + "/cemeteries", cemetery);
    }

    function editCemetery(cemetery) {
        return $http.put(config.remoteServiceName + "/cemeteries", cemetery);
    }

    function deleteCemetery(cemeteryId) {

    }

    function getGraves() {
//        var graves = [
//            {
//                id: 2221313,
//                number: 12222,
//                cemeteryName: 'Cimitir',
//                cemeteryId: 123,
//                plotNumber: 1235,
//                surface: 'suprafata',
//                inhumations: [
//                    {
//                        nopermit: 1234,
//                        graveId: 1234,
//                        date: '1-2-2302',
//                        noreceipt: 1234,
//                        deceased: {
//                            ssn: 13123,
//                            firstName: 'firstname',
//                            lastName: 'lastname',
//                            religion: 'religion',
//                            isAbandoned: 'abandoned'
//                        }
//                    }
//                ],
//                isMonument: false,
//                additionalInfo: 'info',
//                image: 'http://thomasgenweb.com/John_J_Thomas_grave.jpg'
//            },
//            {
//                id: 122122,
//                number: 3333,
//                cemeteryName: 'Cimitir1',
//                cemeteryId: 123,
//                plotNumber: 222,
//                surface: 'suprafata1',
//                inhumations: [
//                    {
//                        nopermit: 2232,
//                        graveId: 1234,
//                        date: '1-2-23012',
//                        noreceipt: 1234,
//                        deceased: {
//                            ssn: 13123,
//                            firstName: 'firstname1',
//                            lastName: 'lastnam2e',
//                            religion: 'religi2on',
//                            isAbandoned: 'aband1oned'
//                        }
//                    }
//                ],
//                isMonument: false,
//                additionalInfo: 'info1',
//                image: 'http://thomasgenweb.com/John_J_Thomas_grave.jpg'
//            }
//        ];
//        this.graves = graves;
//        return $q.when(graves);
        return $resource(config.remoteServiceName + "/grave", {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getGraveById(graveId) {
            var foundGrave = [];
            if (this.graves) {
                this.graves.forEach(function (grave) {
                    if (grave.id == graveId) {
                        foundGrave = grave;
                    }
                });
            }
            return $q.when(foundGrave);
        }

        function deleteGrave(graveId) {
            return $http.delete(config.remoteServiceName + "/grave/"+graveId);
        }

        function editGrave(grave) {
            return $http.put(config.remoteServiceName + "/grave", grave);
        }

        function addGrave(grave) {
            return $http.post(config.remoteServiceName + "/grave", grave);
        }

        function uploadImage(data) {
            return $http.post(serviceBase + 'api/', data, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            }).then(function (result) {
                return result;
            });
        }

        function addIntimation(intimation) {
            return $http.post(config.remoteServiceName + "/intimation", intimation);
        }

        // End Management

        // Start Inhumation

        function getDeceased() {
//            Mock data
//            var deceased = [
//                { ssn: '12351', firstName: 'FirstName1', lastName: 'LastName1', religion: 'religion', isAbandoned: false },
//                { ssn: '12352', firstName: 'FirstName2', lastName: 'LastName2', religion: 'religion', isAbandoned: false },
//                { ssn: '12353', firstName: 'FirstName3', lastName: 'LastName3', religion: 'religion', isAbandoned: false },
//                { ssn: '12354', firstName: 'FirstName4', lastName: 'LastName4', religion: 'religion', isAbandoned: true },
//                { ssn: '12355', firstName: 'FirstName5', lastName: 'LastName5', religion: 'religion', isAbandoned: true }
//            ];
//            this.deceased = deceased;
//            return $q.when(deceased);

            return $resource(config.remoteServiceName + '/deceased', {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getDeceasedById(deceasedId) {
            var foundDeceased = {};
            if (this.deceased) {
                this.deceased.forEach(function (deceased) {
                    if (deceased.ssn == deceasedId) {
                        foundDeceased = deceased;
                    }
                });
            }
            return $q.when(foundDeceased);
        }

        function editDeceased(deceased) {
            return $http.put(config.remoteServiceName + "/deceased", deceased);
        }

        function getBurials() {
//            var burials = [
//                {
//                    nopermit: 1234,
//                    graveId: 1234,
//                    cemeteryName: 'cemetery',
//                    graveNumber: 1234,
//                    date: '4-2-20120',
//                    noreceipt: 1234,
//                    deceased: {
//                        ssn: 13123,
//                        firstName: 'FirstName',
//                        lastName: 'lastname',
//                        religion: 'rel',
//                        isAbandoned: false
//                    }
//                },
//                {
//                    nopermit: 123333,
//                    graveId: 2222,
//                    cemeteryName: 'cemetery',
//                    graveNumber: 1234,
//                    date: '4-2-20120',
//                    noreceipt: 1234,
//                    deceased: {
//                        ssn: 13123,
//                        firstName: 'FirstN122ame',
//                        lastName: 'lastname22',
//                        religion: 'rel122',
//                        isAbandoned: false
//                    }
//                }
//            ];
//            return $q.when(burials);
            return $resource(config.remoteServiceName + '/inhumation', {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function addBurial(burial) {
            return $http.post(config.remoteServiceName + "/inhumation", burial);
        }

        // End Inhumation

        // Start Registries
        function getInhumationRegistry() {
//            var scheduleDeceasedRegistryEntries = [
//                { firstName: "2", lastName: "lastname1", religion: "religion1", cemeteryName: "cemetery1", plotNumber: "plot1", date: "data1" },
//                { firstName: "na2me2", lastName: "lastname2", religion: "religion2", cemeteryName: "cemetery1", plotNumber: "plot2", date: "data2" },
//                { firstName: "n2ame3", lastName: "lastname3", religion: "religion3", cemeteryName: "cemetery1", plotNumber: "plot3", date: "data3" },
//                { firstName: "name4", lastName: "lastname4", religion: "religion4", cemeteryName: "cemetery1", plotNumber: "plot4", date: "data4" },
//                { firstName: "name5", lastName: "lastname5", religion: "religion5", cemeteryName: "cemetery1", plotNumber: "plot5", date: "data5" }
//            ];
//            return $q.when(scheduleDeceasedRegistryEntries);
            return $resource(config.remoteServiceName + '/InhumationRegistry', {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getGraveRegistry() {
//            var graveRegistryEntries = [
//                { cemeteryName: "name1", plotNumber: "plotnr1", graveNumber: "gravenr1", ownerName: "name1", ownerAddress: "address1", receiptNumber: "number1", deceasedName: "deceasedName1", burialDate: "burialDate", surface: "surface1", observations: "observations1" },
//                { cemeteryName: "name2", plotNumber: "plotnr2", graveNumber: "gravenr2", ownerName: "name2", ownerAddress: "address2", receiptNumber: "number2", deceasedName: "deceasedName2", burialDate: "burialDate", surface: "surface2", observations: "observations2" },
//                { cemeteryName: "name3", plotNumber: "plotnr3", graveNumber: "gravenr3", ownerName: "name3", ownerAddress: "address3", receiptNumber: "number3", deceasedName: "deceasedName3", burialDate: "burialDate", surface: "surface3", observations: "observations3" },
//                { cemeteryName: "name4", plotNumber: "plotnr4", graveNumber: "gravenr4", ownerName: "name4", ownerAddress: "address4", receiptNumber: "number4", deceasedName: "deceasedName4", burialDate: "burialDate", surface: "surface4", observations: "observations4" },
//                { cemeteryName: "name5", plotNumber: "plotnr5", graveNumber: "gravenr5", ownerName: "name5", ownerAddress: "address5", receiptNumber: "number5", deceasedName: "deceasedName5", burialDate: "burialDate", surface: "surface5", observations: "observations5" }
//            ];
//            return $q.when(graveRegistryEntries);
            return $resource(config.remoteServiceName + '/GraveRegistry', {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getGraveMonumentsRegistry() {
//            var graveMonumentsRegistryEntries = [
//                { cemeteryName: "name1", plotNumber: "plotnr1", graveNumber: "gravenr1", ownerName: "name1", ownerAddress: "address1", receiptNumber: "number1", deceasedName: "deceasedName1", burialDate: "burialDate", surface: "surface1", observations: "observations1" },
//                { cemeteryName: "name2", plotNumber: "plotnr2", graveNumber: "gravenr2", ownerName: "name2", ownerAddress: "address2", receiptNumber: "number2", deceasedName: "deceasedName2", burialDate: "burialDate", surface: "surface2", observations: "observations2" },
//                { cemeteryName: "name3", plotNumber: "plotnr3", graveNumber: "gravenr3", ownerName: "name3", ownerAddress: "address3", receiptNumber: "number3", deceasedName: "deceasedName3", burialDate: "burialDate", surface: "surface3", observations: "observations3" },
//                { cemeteryName: "name4", plotNumber: "plotnr4", graveNumber: "gravenr4", ownerName: "name4", ownerAddress: "address4", receiptNumber: "number4", deceasedName: "deceasedName4", burialDate: "burialDate", surface: "surface4", observations: "observations4" },
//                { cemeteryName: "name5", plotNumber: "plotnr5", graveNumber: "gravenr5", ownerName: "name5", ownerAddress: "address5", receiptNumber: "number5", deceasedName: "deceasedName5", burialDate: "burialDate", surface: "surface5", observations: "observations5" }
//            ];
//            return $q.when(graveMonumentsRegistryEntries);
            return $resource(config.remoteServiceName + '/GraveRegistry/monuments', {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getDeceasedRegistry() {
//            var deceasedRegistryEntries = [
//                { firstName: "name1", lastName: "lastname1", cemeteryName: "cemetery1", plotNumber: "plot1", graveNr: "graveNumber1" },
//                { firstName: "name2", lastName: "lastname2", cemeteryName: "cemetery1", plotNumber: "plot2", graveNr: "graveNumber2" },
//                { firstName: "name3", lastName: "lastname3", cemeteryName: "cemetery1", plotNumber: "plot3", graveNr: "graveNumber3" },
//                { firstName: "name4", lastName: "lastname4", cemeteryName: "cemetery1", plotNumber: "plot4", graveNr: "graveNumber4" },
//                { firstName: "name5", lastName: "lastname5", cemeteryName: "cemetery1", plotNumber: "plot5", graveNr: "graveNumber5" }
//            ];
//            return $q.when(deceasedRegistryEntries);
            return $resource(config.remoteServiceName + '/deceasedRegistry/2015', {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getAbandonedDeceasedRegistry() {
//            var abandonedDeceasedRegistryEntries = [
//                { cemeteryName: "name1", inhumationReceipt: "inhumationReceipt1", socialAssistence: "socialAssistence1" },
//                { cemeteryName: "name2", inhumationReceipt: "inhumationReceipt2", socialAssistence: "socialAssistence2" },
//                { cemeteryName: "name3", inhumationReceipt: "inhumationReceipt3", socialAssistence: "socialAssistence3" }
//            ];
//            return $q.when(abandonedDeceasedRegistryEntries);
            return $resource(config.remoteServiceName + '/deceasedRegistry/abandoned/2015', {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getInhumationRequestRegistry() {
//            var applicationRegistryEntries = [
//                { currentNr: "1", registrationDate: "registrationDate", nrInfocet: "1212", state: "in curs de procesare" },
//                { currentNr: "2", registrationDate: "registrationDate", nrInfocet: "1232", state: "in curs de procesare" },
//                { currentNr: "3", registrationDate: "registrationDate", nrInfocet: "1252", state: "in curs de procesare" },
//                { currentNr: "4", registrationDate: "registrationDate", nrInfocet: "1222", state: "in curs de procesare" }
//            ];
//            return $q.when(applicationRegistryEntries);
            return $resource(config.remoteServiceName + '/contractRegistry/applications', {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }

        function getPurchaseContractRegistry() {
//            var purchaseContractRegistryEntries = [
//                { currentNr: "1", contractNr: "1111", contractDate: "data1", lastName: "nume1", firstName: "in curs de procesare", address: "in curs de procesare" },
//                { currentNr: "2", contractNr: "2222", contractDate: "data1", lastName: "nume5e", firstName: "in curs de procesare", address: "in curs de procesare" },
//                { currentNr: "3", contractNr: "3333", contractDate: "data1", lastName: "nume4e", firstName: "in curs de procesare", address: "in curs de procesare" },
//                { currentNr: "4", contractNr: "4444", contractDate: "data1", lastName: "nume3", firstName: "in curs de procesare", address: "in curs de procesare" },
//                { currentNr: "5", contractNr: "5555", contractDate: "data1", lastName: "nume2", firstName: "in curs de procesare", address: "in curs de procesare" }
//            ];
//            return $q.when(purchaseContractRegistryEntries);

            return $resource(config.remoteServiceName + '/contractRegistry/2015', {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }


        function getIntimationRegistry() {
            var intimationRegistryEntries = [
                { currentNr: "1", lastName: "lname1", firstName: "fname1", observations: "obs1" },
                { currentNr: "2", lastName: "lname2", firstName: "fname2s", observations: "obs2" }
            ];
            return $q.when(intimationRegistryEntries);
        }
        // End Registries

        // Start Export Registries
        function exportInhumationRegistry() {
            window.location.href = config.remoteServiceName + '/InhumationRegistry/export';
        }

        function exportGraveRegistry() {
            window.location.href = config.remoteServiceName + '/GraveRegistry/export';
        }

        function exportGraveMonumentsRegistry() {
            window.location.href = config.remoteServiceName + '/GraveRegistry/export';
        }

        function exportDeceasedRegistry() {
            window.location.href = config.remoteServiceName + '/deceasedRegistry/export';
        }

        function exportAbandonedDeceasedRegistry() {
            window.location.href = config.remoteServiceName + '/deceasedRegistry/abandoned/export/2015';
        }

        function exportInhumationRequestRegistry() {
            window.location.href = config.remoteServiceName + '/contractRegistry/applications/export';
        }

        function exportPurchaseContractRegistry() {
            window.location.href = config.remoteServiceName + '/contractRegistry/export/2015';
        }

        function exportIntimationRegistry() {
            window.location.href = config.remoteServiceName + '/IntimationRegistry/export';
        }
        // End Export Registries

        function getEntityChanges() {
//            var changes = [
//                {id: 1, userName: 'name1', documentNumber: 12314, detailInfo: 'info1', date: '2014-12-16 13:22:44'},
//                {id: 2, userName: 'name2', documentNumber: 222, detailInfo: 'info2', date: '2014-12-16 13:22:44'},
//                {id: 3, userName: 'name3', documentNumber: 333, detailInfo: 'info3', date: '2014-12-16 13:22:44'},
//                {id: 4, userName: 'name4', documentNumber: 444, detailInfo: 'info4', date: '2014-12-16 13:22:44'}
//            ];
//            return $q.when(changes);
            return $resource(config.remoteServiceName + '/entitychanges', {}, {'query': {method: 'GET', isArray: true}}).query().$promise;
        }
    }
})();