﻿(function () {
    'use strict';

    var app = angular.module('app');

    // Collect the routes
    app.constant('routes', getRoutes());
    
    // Configure the routes and route resolvers
    app.config(['$routeProvider', 'routes', routeConfigurator]);
    function routeConfigurator($routeProvider, routes) {

        routes.forEach(function (r) {
            setRoute(r.url, r.config);
        });
        $routeProvider.otherwise({ redirectTo: '/' });

        function setRoute(url, definition){
            definition.resolve = angular.extend(definition.resolve || {}, {
                // Add resolvers for all routes
            });
            $routeProvider.when(url, definition);
            return $routeProvider;
        }
    }

    dummyReject.$inject = ['$q'];
    function dummyReject($q){
        var defer = $q.defer();
        defer.reject({msg: 'You shall not pass'});
        return defer.promise;
    }

    // Define the routes 
    function getRoutes() {
        return [
//            {
//                url: '/dashboard',
//                config: {
//                    templateUrl: 'app/modules/dashboard/dashboard.html',
//                    title: 'dashboard',
//                    settings: {
//                        nav: 1,
//                        content: 'Dashboard'
//                    }
//                }
//            },
            {
                url: '/applications',
                config: {
                    title: 'applications',
                    templateUrl: 'app/modules/applications/applications.html',
                    settings: {
                        nav: 1,
                        content: 'Cereri'
                    }
                }
            },
            {
                url: '/application/:id?/edit/:edit',
                config: {
                    title: 'Application',
                    templateUrl: 'app/modules/applications/applicationDetail/applicationDetail.html',
                    settings: { }
                }
            },
            {
                url: '/purchases',
                config: {
                    title: 'purchases',
                    templateUrl: 'app/modules/purchases/purchases.html',
                    settings: {
                        nav: 2,
                        content: 'Concesionari'
                    }
                }
            },
            {
                url: '/purchase/:id?/edit/:edit',
                config: {
                    title: 'Purchase',
                    templateUrl: 'app/modules/purchases/purchaseDetail/purchaseDetail.html',
                    settings: { }
                }
            },
            {
                url: '/registries',
                config: {
                    title: 'registries',
                    templateUrl: 'app/modules/registries/registries.html',
                    settings: {
                        nav: 3,
                        content: 'Registre'
                    }
//                    resolve: {
//                        // we can have multiple resolvers
//                        dummy: dummyReject
//                    }
                }
            },
            {
                url: '/management',
                config: {
                    title: 'management',
                    templateUrl: 'app/modules/management/management.html',
                    settings: {
                        nav: 4,
                        content: 'Management'
                    }
                }
            },
            {
                url: '/cemeteryinfo/:id',
                config: {
                    title: 'Cemetery',
                    templateUrl: 'app/modules/management/cemeteryInfo.html',
                    settings: { }
                }
            },
            {
                url: '/grave/:id?/edit/:edit',
                config: {
                    title: 'Grave',
                    templateUrl: 'app/modules/management/details/graveDetail/graveDetail.html',
                    settings: { }
                }
            },
            {
                url: '/citizen/:id?/edit/:edit',
                config: {
                    title: 'Citizen',
                    templateUrl: 'app/modules/management/details/citizenDetail/citizenDetail.html',
                    settings: { }
                }
            },
            {
                url: '/cemetery/:id?/edit/:edit',
                config: {
                    title: 'Cemetery',
                    templateUrl: 'app/modules/management/details/cemeteryDetail/cemeteryDetail.html',
                    settings: { }
                }
            },
            {
                url: '/intimation/:id?/edit/:edit',
                config: {
                    title: 'Intimation',
                    templateUrl: 'app/modules/management/details/intimationDetail/intimationDetail.html',
                    settings: { }
                }
            },
            {
                url: '/inhumations',
                config: {
                    title: 'inhumations',
                    templateUrl: 'app/modules/inhumations/inhumations.html',
                    settings: {
                        nav: 5,
                        content: 'Inhumari'
                    }
                }
            },
            {
                url: '/deceased/:id?/edit/:edit',
                config: {
                    title: 'Deceased',
                    templateUrl: 'app/modules/inhumations/details/deceasedDetail/deceasedDetail.html',
                    settings: { }
                }
            },
            {
                url: '/inhumation/:id?/edit/:edit',
                config: {
                    title: 'Inhumation',
                    templateUrl: 'app/modules/inhumations/details/inhumationDetail/inhumationDetail.html',
                    settings: { }
                }
            },
            {
                url: '/changes',
                config: {
                    title: 'changes',
                    templateUrl: 'app/modules/entity-changes/entityChanges.html',
                    settings: {
                        nav: 6,
                        content: 'Modificari'
                    }
                }
            }
        ];
    }
})();