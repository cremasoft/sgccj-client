﻿(function () {
    'use strict';

    var app = angular.module('app');

    // Configure Toastr
    toastr.options.timeOut = 4000;
    toastr.options.positionClass = 'toast-bottom-right';

    var remoteServiceName = 'http://localhost:26264/api';

    var events = {
        controllerActivateSuccess: 'controller.activateSuccess',
        spinnerToggle: 'spinner.toggle',
        authLoginSuccess: 'auth.loginSuccess'
    };

    var config = {
        appErrorPrefix: '[Debug Error] ', //Configure the exceptionHandler decorator
        docTitle: 'SGCCJ: ',
        events: events,
        remoteServiceName: remoteServiceName,
        version: '0.1'
    };

    app.value('config', config);
    
    app.config(['$logProvider', function ($logProvider) {
        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(false);
        }
    }]);
    
    //#region Configure the common services via commonConfig
    app.config(['commonConfigProvider', function (cfg) {
        cfg.config.controllerActivateSuccessEvent = config.events.controllerActivateSuccess;
        cfg.config.spinnerToggleEvent = config.events.spinnerToggle;
        cfg.config.authLoginSuccessEvent = config.events.authLoginSuccess;
    }]);
    //#endregion
})();