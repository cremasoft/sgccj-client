﻿(function () {
    'use strict';

    var app = angular.module('app', [
        // Angular modules
        'ngResource',           // resource
        'ngAnimate',            // animations
        'ngRoute',              // routing
        'ngSanitize',           // sanitizes html bindings (ex: sidebar.js)
        'LocalStorageModule',   // local storage
        'ngGrid',               // Ng Grid

        // Custom modules
        'common',           // common functions, logger, spinner
        'common.bootstrap', // bootstrap dialog wrapper functions

        // 3rd Party Modules
        'ui.bootstrap',     // ui-bootstrap (ex: carousel, pagination, dialog)
        'angular-confirm'
    ]);

    // Handle routing errors and success events
    app.run(['$route', 'routeMediator',  function ($route, routeMediator) {
        // Include $route to kick start the router.
        routeMediator.updateDocTitle();
    }]);

    app.config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    });
})();