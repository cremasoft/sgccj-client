﻿(function () {
    'use strict';
    var controllerId = 'dashboard';
    angular.module('app').controller(controllerId, ['authService', 'common', 'datacontext', dashboard]);

    function dashboard(authService, common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId, 'success');

        var vm = this;
        vm.title = 'Dashboard';

        activate();

        function activate() {
            var promises = [useMockData()];
            common.activateController(promises, controllerId)
                .then(function () {});
        }

        function initializeMap(locations){
            var center = new google.maps.LatLng(46.777273, 23.596943);

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 5,
                center: center,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                disableDoubleClickZoom: true,
                zoomControlOptions: true,
                streetViewControl: false
            });

            var markers = [];
            locations.forEach(function(location){
                var latLng = new google.maps.LatLng(location.latitude, location.longitude);
                var marker = new google.maps.Marker({position: latLng});
                markers.push(marker);
            });
            var markerCluster = new MarkerClusterer(map, markers);
        }

        function addIdToLocations(locations){
            var index = 1;
            locations.forEach(function(location){
                location.id = index++;
            });
        }

        function useMockData(){
            var mockLocations = [
                { latitude: '46.777273', longitude: '23.596943', id: 1},
                { latitude: '46.755239', longitude: '23.547663', id: 2},
                { latitude: '46.756650', longitude: '23.553199', id: 3},
                { latitude: '46.754063', longitude: '23.554465', id: 4},
                { latitude: '46.754445', longitude: '23.560022', id: 5},
                { latitude: '46.777712', longitude: '23.611242', id: 6},
                { latitude: '46.782282', longitude: '23.632334', id: 7},
                { latitude: '46.774406', longitude: '23.629202', id: 8}
            ];
            initializeMap(mockLocations);
        }
    }
})();