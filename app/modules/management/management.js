﻿(function () {
    'use strict';
    var controllerId = 'management';
    angular.module('app').controller(controllerId, ['$location','$confirm', 'common', 'datacontext', '$scope', management]);

    function management($location, $confirm, common, datacontext,$scope) {
        var getLogFn = common.logger.getLogFn;
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Management';

        vm.goToCemeteryInfo = goToCemeteryInfo;
        vm.goToGraveDetail = goToGraveDetail;
        vm.goToCitizenDetail = goToCitizenDetail;
        vm.goToCemeteryDetail = goToCemeteryDetail;

        vm.citizensFilterFnc = citizensFilterFnc;
        vm.citizensAddFnc = citizensAddFnc;
        vm.citizensEditFnc = citizensEditFnc;
        vm.citizensSelectedEntry = [];
        vm.citizensFilterOptions = {filterText: ''};
        $scope.citizens = [];
        $scope.citizensGridOptions = {
            data: 'citizens',
            selectedItems: vm.citizensSelectedEntry,
            multiSelect: false,
            keepLastSelected: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.citizensFilterOptions,
            columnDefs: [
                {field: 'ssn', displayName: 'CNP'},
                {field: 'lastName', displayName:'Nume'},
                {field: 'firstName', displayName:'Prenume'},
                {field: 'address', displayName:'Adresa'}
            ]
        };

        vm.cemeteriesFilterFnc = cemeteriesFilterFnc;
        vm.cemeteriesAddFnc = cemeteriesAddFnc;
        vm.cemeteriesEditFnc = cemeteriesEditFnc;
        vm.cemeteriesSelectedEntry = [];
        vm.cemeteriesFilterOptions = {filterText: ''};
        vm.cemeteryDblClickHandler = cemeteryDblClickHandler;
        $scope.cemeteries = [];
        $scope.cemeteriesGridOptions = {
            data: 'cemeteries',
            selectedItems: vm.cemeteriesSelectedEntry,
            multiSelect: false,
            keepLastSelected: true,
            enableColumnResize: true,
            dblClickFn: vm.cemeteryDblClickHandler,
            plugins: [new ngGridFlexibleHeightPlugin(), new ngGridDoubleClick()],
            filterOptions: vm.cemeteriesFilterOptions,
            columnDefs: [
                {field: 'name', displayName: 'Nume'},
                {field: 'address', displayName:'Adresa'},
                {field: 'intimations.length', displayName:'Sesizari'},
                {field: 'plots.length', displayName:'Parcele'},
                {field: 'graves.length', displayName:'Locuri de veci'}
            ]
        };

        vm.gravesFilterFnc = gravesFilterFnc;
        vm.gravesAddFnc = gravesAddFnc;
        vm.gravesEditFnc = gravesEditFnc;
        vm.gravesDeleteFnc = gravesDeleteFnc;
        vm.gravesSelectedEntry = [];
        vm.gravesFilterOptions = {filterText: ''};
        vm.gravesDblClickHandler = gravesDblClickHandler;
        $scope.graves = [];
        $scope.gravesGridOptions = {
            data: 'graves',
            selectedItems: vm.gravesSelectedEntry,
            multiSelect: false,
            keepLastSelected: true,
            enableColumnResize: true,
            dblClickFn: vm.gravesDblClickHandler,
            plugins: [new ngGridFlexibleHeightPlugin(), new ngGridDoubleClick()],
            filterOptions: vm.gravesFilterOptions,
            columnDefs: [
                {field: 'cemeteryName', displayName:'Nume cimitir'},
                {field: 'number', displayName:'Numar'},
                {field: 'plotNumber', displayName:'Numar parcela'},
                {field: 'surface', displayName: 'Suprafata'},
                {field: 'burials.length', displayName: 'Inmormantari'},
                {field: 'isMonument', displayName:'Monument'},
                {field: 'additionalInfo', displayName:'Informatii'}
            ]
        };

        activate();

        function activate() {
            var promises = [getCemeteries(), getCitizens(), getGraves()];
            common.activateController(promises, controllerId)
                .then(function () {});
        }

        function citizensFilterFnc(filterText) {
            vm.citizensFilterOptions.filterText = filterText;
        }

        function citizensAddFnc() {
            vm.goToCitizenDetail(false);
        }

        function citizensEditFnc() {
            if (vm.citizensSelectedEntry.length > 0) {
                vm.goToCitizenDetail(true, vm.citizensSelectedEntry[0]);
            }
        }

        function cemeteriesFilterFnc(filterText) {
            vm.cemeteriesFilterOptions.filterText = filterText;
        }

        function cemeteriesAddFnc() {
            vm.goToCemeteryDetail(false);
        }

        function cemeteriesEditFnc() {
            if (vm.cemeteriesSelectedEntry.length > 0) {
                vm.goToCemeteryDetail(true, vm.cemeteriesSelectedEntry[0]);
            }
        }

        function cemeteryDblClickHandler(rowItem) {
            vm.goToCemeteryInfo(rowItem);
        }

        function gravesAddFnc(){
            vm.goToGraveDetail(false);
        }

        function gravesEditFnc(){
            if (vm.gravesSelectedEntry.length > 0) {
                vm.goToGraveDetail(true, vm.gravesSelectedEntry[0]);
            }
        }

        function gravesDeleteFnc(){
            if (vm.gravesSelectedEntry.length > 0) {
                $confirm({text: 'Sunteti sigur ca doriti sa stergeti acest loc de veci?'})
                    .then(function () {
                        datacontext.deleteGrave(vm.gravesSelectedEntry[0].id).then(
                            function(data){
                                logSuccess('Operatiunea a avut success!', null, true);
                                getGraves();
                            },
                            function(error){
                                logError('Va rugam incercati din nou.', null, true);
                            }
                        );
                    });
            }
        }

        function gravesDblClickHandler(rowItem) {
            vm.goToGraveDetail(false, rowItem);
        }

        function gravesFilterFnc(filterText) {
            vm.gravesFilterOptions.filterText = filterText;
        }

        function getCitizens() {
            return datacontext.getCitizens().then(
                function (response) {
                    // success
                    $scope.citizens = response;
                    datacontext.citizens = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function getCemeteries() {
            return datacontext.getCemeteries().then(
                function (response) {
                    // success
                    $scope.cemeteries = response;
                    datacontext.cemeteries = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function getGraves() {
            return datacontext.getGraves().then(
                function (response) {
                    // success
                    $scope.graves = response;
                    datacontext.graves = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }
        
        function goToCemeteryInfo(cemetery){
            if (cemetery){
                $location.path('/cemeteryinfo/' + cemetery.id);
            }
        }

        function goToGraveDetail(editMode,grave){
            if (grave == undefined){
                // Open in add mode
                $location.path('/grave' + '/edit/' + false);
            } else if (editMode) {
                // Open in edit mode
                $location.path('/grave/' + grave.id + '/edit/' + true);
            } else {
                // Open in view mode
                $location.path('/grave/' + grave.id + '/edit/' + false);
            }
        }

        function goToCemeteryDetail(editMode,cemetery){
            if (cemetery == undefined){
                // Open in add mode
                $location.path('/cemetery' + '/edit/' + false);
            } else if (editMode) {
                // Open in edit mode
                $location.path('/cemetery/' + cemetery.id + '/edit/' + true);
            } else {
                // Open in view mode
                $location.path('/cemetery/' + cemetery.id + '/edit/' + false);
            }
        }

        function goToCitizenDetail(editMode, citizen) {
            if (editMode) {
                // Open in edit mode
                $location.path('/citizen/' + citizen.ssn + '/edit/' + true);
            } else {
                // Open in add mode
                $location.path('/citizen' + '/edit/' + false);
            }
        }


    }
})();