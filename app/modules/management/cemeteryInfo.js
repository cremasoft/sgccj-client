﻿(function () {
    'use strict';
    var controllerId = 'cemeteryInfo';
    angular.module('app').controller(controllerId, ['$location', '$confirm', '$routeParams', '$window', 'common', 'datacontext', '$scope', cemeteryInfo]);

    function cemeteryInfo($location, $confirm, $routeParams, $window, common, datacontext,$scope) {
        var getLogFn = common.logger.getLogFn;
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Cemetery Info';
        vm.cemetery = {};
        vm.goBack = goBack;
        vm.goToGraveDetail = goToGraveDetail;
        vm.goToIntimationDetail = goToIntimationDetail;

        vm.intimationsFilterFnc = intimationsFilterFnc;
        vm.intimationsAddFnc = intimationsAddFnc;
        vm.intimationsFilterOptions = {filterText: ''};
        $scope.intimations = [];
        $scope.intimationsGridOptions = {
            data: 'intimations',
            enableRowSelection: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.intimationsFilterOptions,
            columnDefs: [
                {field: 'description', displayName: 'Informatii'},
                {field: 'date', displayName: 'Data'}
            ]
        };


        vm.plotsFilterFnc = plotsFilterFnc;
        vm.plotsAddFnc = plotsAddFnc;
        vm.plotsFilterOptions = {filterText: ''};
        $scope.plots = [];
        $scope.plotsGridOptions = {
            data: 'plots',
            enableRowSelection: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.plotsFilterOptions,
            columnDefs: [
                {field: 'number', displayName: 'Numar'},
                {field: 'nrOfGraves', displayName:'Locuri de veci'}
            ]
        };

        vm.gravesFilterFnc = gravesFilterFnc;
        vm.gravesAddFnc = gravesAddFnc;
        vm.gravesEditFnc = gravesEditFnc;
        vm.gravesDeleteFnc = gravesDeleteFnc;
        vm.gravesSelectedEntry = [];
        vm.gravesDblClickHandler = gravesDblClickHandler;
        vm.gravesFilterOptions = {filterText: ''};
        $scope.graves = [];
        $scope.gravesGridOptions = {
            data: 'graves',
            selectedItems: vm.gravesSelectedEntry,
            multiSelect: false,
            keepLastSelected: true,
            enableColumnResize: true,
            dblClickFn: vm.gravesDblClickHandler,
            plugins: [new ngGridFlexibleHeightPlugin(), new ngGridDoubleClick()],
            filterOptions: vm.gravesFilterOptions,
            columnDefs: [
                {field: 'cemeteryName', displayName:'Nume cimitir'},
                {field: 'number', displayName:'Numar'},
                {field: 'plotNumber', displayName:'Numar parcela'},
                {field: 'surface', displayName: 'Suprafata'},
                {field: 'burials.length', displayName: 'Inmormantari'},
                {field: 'isMonument', displayName:'Monument'},
                {field: 'additionalInfo', displayName:'Informatii'}
            ]
        };

        activate();

        function activate() {
            var promises = [getCemetery()];
            common.activateController(promises, controllerId)
                .then(function () { });
        }

        function getCemetery() {
            var cemeteryId = $routeParams.id;
            console.log(cemeteryId);
            datacontext.getCemeteryById(cemeteryId).then(
                function (response) {
                    // success
                    vm.cemetery = response;
                    $scope.plots = response.plots;
                    $scope.intimations = response.intimations;
                    $scope.graves = response.graves;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function intimationsAddFnc() {
            vm.goToIntimationDetail(false);
        }

        function intimationsFilterFnc(filterText) {
            vm.intimationsFilterOptions.filterText = filterText;
        }

        function plotsAddFnc() {

        }

        function plotsFilterFnc(filterText) {
            vm.plotsFilterOptions.filterText = filterText;
        }

        function gravesAddFnc(){
            vm.goToGraveDetail(false);
        }

        function gravesEditFnc(){
            if (vm.gravesSelectedEntry.length > 0) {
                vm.goToGraveDetail(true, vm.gravesSelectedEntry[0]);
            }
        }

        function gravesDeleteFnc(){
            if (vm.gravesSelectedEntry.length > 0) {
                $confirm({text: 'Sunteti sigur ca doriti sa stergeti acest loc de veci?'})
                    .then(function () {
                    datacontext.deleteGrave(vm.grave.id).then(
                        function(data){
                            logSuccess('Operatiunea a avut success!', null, true);
                        },
                        function(error){
                            logError('Va rugam incercati din nou.', null, true);
                        }
                    );
                        logSuccess('Operatiunea a avut success!', null, true);
                    });
            }
        }

        function gravesFilterFnc(filterText) {
            vm.gravesFilterOptions.filterText = filterText;
        }

        function gravesDblClickHandler(rowItem) {
            vm.goToGraveDetail(false, rowItem);
        }

        function goToGraveDetail(editMode,grave){
            if (grave == undefined){
                // Open in add mode
                $location.path('/grave' + '/edit/' + false);
            } else if (editMode) {
                // Open in edit mode
                $location.path('/grave/' + grave.id + '/edit/' + true);
            } else {
                // Open in view mode
                $location.path('/grave/' + grave.id + '/edit/' + false);
            }
        }

        function goToIntimationDetail(editMode){
            $location.path('/intimation/'+ vm.cemetery.id + '/edit/' + false);
        }

        function goBack(){
            $window.history.back();
        }
    }
})();