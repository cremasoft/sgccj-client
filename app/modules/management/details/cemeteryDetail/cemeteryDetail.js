(function () {
    'use strict';
    var controllerId = 'cemeteryDetail';
    angular.module('app').controller(controllerId, ['$scope', '$routeParams','$confirm', '$window', 'common', 'datacontext', cemeteryDetail]);

    function cemeteryDetail($scope, $routeParams,$confirm, $window, common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Cemetery Detail';

        vm.cemeteryIdParameter = $routeParams.id;
        vm.isInEditMode = $routeParams.edit;
        vm.displayMessage = '';
        vm.detailMode = 'VIEW';

        vm.cemetery = {};

        vm.goBack = goBack;
        vm.saveCemetery = saveCemetery;

        activate();

        function activate() {
            var promises = [setupDetailView(), getCemetery()];
            common.activateController(promises, controllerId)
                .then(function () {
//                    log('Activated Grave detail View');
                });
        }

        function getCemetery() {
            if (vm.detailMode != 'ADD') {
                var cemeteryId = vm.cemeteryIdParameter;
                return datacontext.getCemeteryById(cemeteryId).then(
                    function (data) {
                        vm.cemetery = data;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            }
        }

        function saveCemetery() {
            if (vm.detailMode == 'ADD') {
                datacontext.addCemetery(vm.cemetery).then(
                    function(data){
                        logSuccess('Operatiunea a avut success!', null, true);
                        vm.goBack();
                    },
                    function(error){
                        logError('Va rugam incercati din nou.', null, true);
                    }
                );
            } else {
                datacontext.editCemetery(vm.cemetery).then(
                    function(data){
                        logSuccess('Operatiunea a avut success!', null, true);
                        vm.goBack();
                    },
                    function(error){
                        logError('Va rugam incercati din nou.', null, true);
                    }
                );
            }

        }

        function setupDetailView() {
            if (vm.cemeteryIdParameter == undefined) {
                // Add mode
                vm.displayMessage = "Adaugati un cimitir";
                vm.detailMode = 'ADD';
            } else if (vm.isInEditMode == 'false') {
                // View mode
                vm.displayMessage = "Cimitir";
                vm.detailMode = 'VIEW';
            } else {
                // Edit mode
                vm.displayMessage = "Editati cimitir";
                vm.detailMode = 'EDIT';
            }
        }

        function goBack(){
            $window.history.back();
        }
    }
})();