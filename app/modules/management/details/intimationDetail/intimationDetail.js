(function () {
    'use strict';
    var controllerId = 'intimationDetail';
    angular.module('app').controller(controllerId, ['$scope', '$routeParams','$confirm', '$window', 'common', 'datacontext', intimationDetail]);

    function intimationDetail($scope, $routeParams,$confirm, $window, common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Intimation Detail';

        vm.cemeteryId = $routeParams.id;
        vm.isInEditMode = $routeParams.edit;
        vm.displayMessage = '';
        vm.detailMode = 'VIEW';

        vm.intimation = {};

        vm.goBack = goBack;
        vm.saveIntimation = saveIntimation;

        activate();

        function activate() {
            var promises = [setupDetailView()];
            common.activateController(promises, controllerId)
                .then(function () {
//                    log('Activated Grave detail View');
                });
        }

        function saveIntimation() {
            vm.intimation.cemeteryId = vm.cemeteryId;
                datacontext.addIntimation(vm.intimation).then(
                    function(data){
                        logSuccess('Operatiunea a avut success!', null, true);
                        datacontext.getCemeteries().then(
                            function(data){
                                datacontext.cemeteries = data;
                                vm.goBack();
                            }
                        )
                    },
                    function(error){
                        logError('Va rugam incercati din nou.', null, true);
                    }
                );

        }

        function setupDetailView() {
                // Add mode
                vm.displayMessage = "Adaugati o sesizare";
                vm.detailMode = 'ADD';
        }

        function goBack(){
            $window.history.back();
        }
    }
})();