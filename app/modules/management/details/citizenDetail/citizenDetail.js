(function () {
    'use strict';
    var controllerId = 'citizenDetail';
    angular.module('app').controller(controllerId, ['$scope', '$routeParams','$confirm', '$window', 'common', 'datacontext', citizenDetail]);

    function citizenDetail($scope, $routeParams,$confirm, $window, common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Citizen Detail';

        vm.citizenIdParameter = $routeParams.id;
        vm.isInEditMode = $routeParams.edit;
        vm.displayMessage = '';
        vm.detailMode = 'VIEW';

        vm.citizen = {};

        vm.goBack = goBack;
        vm.saveCitizen = saveCitizen;

        activate();

        function activate() {
            var promises = [setupDetailView(), getCitizen()];
            common.activateController(promises, controllerId)
                .then(function () {
//                    log('Activated Grave detail View');
                });
        }

        function getCitizen() {
            if (vm.detailMode != 'ADD') {
                var citizenId = vm.citizenIdParameter;
                return datacontext.getCitizenById(citizenId).then(
                    function (data) {
                        vm.citizen = data;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            }
        }

        function saveCitizen() {
            if (vm.detailMode == 'ADD') {
                datacontext.addCitizen(vm.citizen).then(
                    function(data){
                        logSuccess('Operatiunea a avut success!', null, true);
                        vm.goBack();
                    },
                    function(error){
                        logError('Va rugam incercati din nou.', null, true);
                    }
                );
            } else {
                datacontext.editCitizen(vm.citizen).then(
                    function(data){
                        logSuccess('Operatiunea a avut success!', null, true);
                        vm.goBack();
                    },
                    function(error){
                        logError('Va rugam incercati din nou.', null, true);
                    }
                );
            }

        }

        function setupDetailView() {
            if (vm.isInEditMode == 'false') {
                // Add mode
                vm.displayMessage = "Adaugati un cetatean";
                vm.detailMode = 'ADD';
            } else {
                // Edit mode
                vm.displayMessage = "Editati cetatean";
                vm.detailMode = 'EDIT';
            }
        }

        function goBack(){
            $window.history.back();
        }
    }
})();