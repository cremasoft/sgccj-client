﻿(function () {
    'use strict';
    var controllerId = 'graveDetail';
    angular.module('app').controller(controllerId, ['$scope', '$routeParams','$confirm', '$window', 'common', 'datacontext', graveDetail]);

    function graveDetail($scope, $routeParams,$confirm, $window, common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Grave Detail';

        vm.cemeteryIdParameter = $routeParams.id;
        vm.isInEditMode = $routeParams.edit;
        vm.displayMessage = '';
        vm.detailMode = 'VIEW';

        vm.grave = {};

        vm.goBack = goBack;
        vm.deleteGrave = deleteGrave;
        vm.saveGrave = saveGrave;

        vm.cemeteries = [];
        vm.selectedCemetery = {};

        activate();

        function activate() {
            var promises = [setupDetailView(), getGrave(), getCemeteries()];
            common.activateController(promises, controllerId)
                .then(function () {
//                    log('Activated Grave detail View');
                });
        }

        $scope.uploadImage = function ($files) {
            console.log('test');
//            var data = new FormData();
//            var file = $files[0];
//            data.append("UploadedImage", file);

//            datacontext.uploadImage(data).then(function (results) {
//                $scope.avatar = 'data:image/png;base64, ' + results.data;
//            }, function (error) {
//                console.log(error);
//            });
        };

        function getGrave() {
            if (vm.detailMode != 'ADD') {
                var graveId = vm.cemeteryIdParameter;
                return datacontext.getGraveById(graveId).then(
                    function (data) {
                        vm.grave = data;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            }
        }

        function getCemeteries() {
            datacontext.getCemeteries().then(
                function (data) {
                    parseCemeteries(data);
                },
                function (error) {
                    console.log(error);
                }
            );
        }

        function parseCemeteries(cemeteries){
            var index = 0;
            cemeteries.forEach(function(cemetery){
                vm.cemeteries[index++] = { label: cemetery.name, value: cemetery.id};
            })
        }

        function saveGrave() {
            if (vm.detailMode == 'ADD') {
                vm.grave.cemeteryName = vm.selectedCemetery.label;
                datacontext.addGrave(vm.grave).then(
                    function(data){
                        logSuccess('Operatiunea a avut success!', null, true);
                        vm.goBack();
                    },
                    function(error){
                        logError('Va rugam incercati din nou.', null, true);
                    }
                );
            } else {
                datacontext.editGrave(vm.grave).then(
                    function(data){
                        logSuccess('Operatiunea a avut success!', null, true);
                        vm.goBack();
                    },
                    function(error){
                        logError('Va rugam incercati din nou.', null, true);
                    }
                );
            }

        }

        function deleteGrave() {
            $confirm({text: 'Sunteti sigur ca doriti sa stergeti acest loc de veci?'})
                .then(function() {
                    datacontext.deleteGrave(vm.grave.id).then(
                        function(data){
                            logSuccess('Operatiunea a avut success!', null, true);
                            vm.goBack();
                        },
                        function(error){
                            logError('Va rugam incercati din nou.', null, true);
                        }
                    );
                    logSuccess('Operatiunea a avut success!', null, true);
                });
        }

        function setupDetailView() {
            if (vm.cemeteryIdParameter == undefined) {
                // Add mode
                vm.displayMessage = "Adaugati un loc de veci";
                vm.detailMode = 'ADD';
            } else if (vm.isInEditMode == 'false') {
                // View mode
                vm.displayMessage = "Loc de veci";
                vm.detailMode = 'VIEW';
            } else {
                // Edit mode
                vm.displayMessage = "Editati loc de veci";
                vm.detailMode = 'EDIT';
            }
        }

        function goBack(){
            $window.history.back();
        }
    }
})();