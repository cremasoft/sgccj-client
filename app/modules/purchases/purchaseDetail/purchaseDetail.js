(function () {
    'use strict';
    var controllerId = 'purchaseDetail';
    angular.module('app').controller(controllerId, ['$scope', '$routeParams','$confirm', '$window', 'common', 'datacontext', purchaseDetail]);

    function purchaseDetail($scope, $routeParams,$confirm, $window, common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Purchase Detail';

        vm.purchaseIdParameter = $routeParams.id;
        vm.isInEditMode = $routeParams.edit;
        vm.displayMessage = '';
        vm.detailMode = 'VIEW';

        vm.purchase = {};

        vm.goBack = goBack;
        vm.savePurchase = savePurchase;

        vm.cemeteries = [];
        vm.selectedCemetery = {};
        vm.plots = [];

        activate();

        function activate() {
            var promises = [setupDetailView(), getPurchase(), getCemeteries()];
            common.activateController(promises, controllerId)
                .then(function () {
//                    log('Activated Grave detail View');
                });
        }

        function getPurchase() {
            if (vm.detailMode != 'ADD') {
                var purchaseId = vm.purchaseIdParameter;
                return datacontext.getPurchaseById(purchaseId).then(
                    function (data) {
                        vm.purchase = data;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            }
        }

        function getCemeteries() {
            datacontext.getCemeteries().then(
                function (data) {
                    parseCemeteries(data);
                },
                function (error) {
                    console.log(error);
                }
            );
        }

        function parseCemeteries(cemeteries){
            var index = 0;
            cemeteries.forEach(function(cemetery){
                vm.cemeteries[index++] = { label: cemetery.name, value: cemetery.id};
            })
        }

        function savePurchase() {
            vm.purchase.cemeteryId = vm.selectedCemetery.value;
            datacontext.addPurchase(vm.purchase).then(
                function(data){
                    logSuccess('Operatiunea a avut success!', null, true);
                    vm.goBack();
                },
                function(error){
                    logError('Va rugam incercati din nou.', null, true);
                }
            );

        }

        function setupDetailView() {
            // Add mode
            vm.displayMessage = "Adaugati un contract";
            vm.detailMode = 'ADD';

        }

        function goBack(){
            $window.history.back();
        }
    }
})();