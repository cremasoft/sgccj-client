(function () {
    'use strict';
    var controllerId = 'purchases';
    angular.module('app').controller(controllerId, ['common', 'datacontext', '$scope', '$location', purchases]);

    function purchases(common, datacontext,$scope,$location) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Purchases';

        vm.goToPurchaseDetail = goToPurchaseDetail;

        vm.purchasesFilterFnc = purchasesFilterFnc;
        vm.purchasesAddFnc = purchasesAddFnc;
        vm.purchasesFilterOptions = {filterText: ''};
        vm.purchasesSelectedEntry = [];
        $scope.purchases = [];
        $scope.purchasesGridOptions = {
            data: 'purchases',
            selectedItems: vm.purchasesSelectedEntry,
            multiSelect: false,
            keepLastSelected: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.purchasesFilterOptions,
            columnDefs: [
                {field: 'contractNumber', displayName: 'Numar contract'},
                {field: 'dateIssued', displayName:'Data'},
                {field: 'ssnOwner', displayName:'CNP'},
                {field: 'lastNameOwner', displayName:'Nume'},
                {field: 'firstNameOwner', displayName:'Prenume'},
                {field: 'addressOwner', displayName:'Adresa'},
                {field: 'cemeteryName', displayName:'Cimitir'},
                {field: 'plotNumber', displayName:'Numar parcela'},
                {field: 'graveNumber', displayName:'Numar loc de veci'},
                {field: 'noReceipt', displayName:'Chitanta'}
            ]
        };


        activate();

        function activate() {
            var promises = [getPurchases()];
            common.activateController(promises, controllerId)
                .then(function () { });
        }

        function purchasesFilterFnc(filterText) {
            vm.purchasesFilterOptions.filterText = filterText;
        }

        function purchasesAddFnc() {
            goToPurchaseDetail(false);
        }

        function goToPurchaseDetail(editMode, purchase) {
            if (editMode) {
                // Open in edit mode
                $location.path('/purchase/' + purchase.contractNumber + '/edit/' + true);
            } else {
                // Open in add mode
                $location.path('/purchase' + '/edit/' + false);
            }
        }

        function getPurchases() {
            return datacontext.getPurchases().then(
                function (response) {
                    // success
                    $scope.purchases = response;
                    datacontext.purchases = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }
    }
})();