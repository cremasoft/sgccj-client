﻿(function () {
    'use strict';

    var controllerId = 'login';
    angular.module('app').controller(controllerId, ['$scope', 'common', '$location', 'authService', 'commonConfig', login]);

    function login($scope, common, $location, authService, commonConfig) {

        var vm = this;
        var logSuccess = common.logger.getLogFn(controllerId, 'success');
        var logError = common.logger.getLogFn('app', 'error');
        vm.loginData = {
            userName: "",
            password: ""
        };
        vm.message = "";
        vm.login = login;

        activate();

        function activate() {
            logSuccess('App started', null, true);
            //common.activateController([authService.fillAuthData()], controllerId);
            common.activateController([], controllerId);
        }

        function login() {
            if (vm.loginData.userName == 'admin' && vm.loginData.password=='admin') {
                authService.authentication.isAuth = true;
                common.$broadcast(commonConfig.config.authLoginSuccessEvent, {});
            }
//            authService.authentication.isAuth = true;
//            common.$broadcast(commonConfig.config.authLoginSuccessEvent, {});
//            authService.login(vm.loginData).then(
//                function (response) {
//                    common.$broadcast(commonConfig.config.authLoginSuccessEvent, {});
//                },
//                function (err) {
//                    $scope.message = err.error_description;
//                    vm.userName = '';
//                    vm.password = '';
//                    logError('Auth failed!', 'Username or password incorrect', true);
//                });
        }
    }
})();