(function () {
    'use strict';
    var controllerId = 'applications';
    angular.module('app').controller(controllerId, ['$rootScope','common','$location','config', 'datacontext', '$scope','authService', applications]);

    function applications($rootScope, common,$location, config,datacontext,$scope,authService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var events = config.events;

        var vm = this;
        vm.title = 'Applications';

        var layoutPlugin = new ngGridLayoutPlugin();
        vm.goToApplicationDetail = goToApplicationDetail;

        vm.activeApplications = 12;
        vm.applicationsFilterFnc = applicationsFilterFnc;
        vm.applicationsAddFnc = applicationsAddFnc;
        vm.applicationsEditFnc = applicationsEditFnc;
        vm.applicationsFilterOptions = {filterText: ''};
        vm.applicationSelectedEntry = [];
        $scope.applications = [];
        $scope.applicationsGridOptions = {
            data: 'applications',
            selectedItems: vm.applicationSelectedEntry,
            multiSelect: false,
            keepLastSelected: false,
            plugins: [new ngGridFlexibleHeightPlugin(), layoutPlugin],
            filterOptions: vm.applicationsFilterOptions,
            sortInfo: {fields: ['infocetNumber'], directions: ['asc']},
            columnDefs: [
                {field: 'infocetNumber', displayName: 'Numar infocet'},
                {field: 'citizenSSN', displayName:'CNP'},
                {field: 'citizenLastName', displayName:'Nume'},
                {field: 'citizenFirstName', displayName:'Prenume'},
                {field: 'citizenAddress', displayName:'Adresa'},
                {field: 'date', displayName:'Data'},
                {field: 'graveNumber', displayName:'Numar mormant'},
                {field: 'stage', displayName:'Stadiu'}
            ]
        };


        activate();

        function activate() {
            if (authService.authentication.isAuth) {
                var promises = [getApplications()];
                common.activateController(promises, controllerId)
                    .then(function () {
                    });
            }
        }

        function applicationsFilterFnc(filterText) {
            vm.applicationsFilterOptions.filterText = filterText;
        }

        function applicationsAddFnc() {
            vm.goToApplicationDetail(false);
        }

        function applicationsEditFnc() {
            if (vm.applicationSelectedEntry.length>0) {
                vm.goToApplicationDetail(true, vm.applicationSelectedEntry[0]);
            }
        }

        function getApplications() {
            return datacontext.getApplications().then(
                function (response) {
                    // success
                    $scope.applications = response;
                    datacontext.applications = response;
                },
                function (response) {
                    // error
                    alert("error");
                    layoutPlugin.updateGridLayout();
                }
            );
        }

        $rootScope.$on(events.authLoginSuccess,
            function (data) {
                window.setTimeout(function(){
                    layoutPlugin.updateGridLayout();
                    activate();
                    console.log(authService.authentication.isAuth);
                }, 10);

            }
        );

        function goToApplicationDetail(editMode, application) {
            if (editMode) {
                // Open in edit mode
                $location.path('/application/' + application.infocetNumber + '/edit/' + true);
            } else {
                // Open in add mode
                $location.path('/application' + '/edit/' + false);
            }
        }
    }
})();