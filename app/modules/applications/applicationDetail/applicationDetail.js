(function () {
    'use strict';
    var controllerId = 'applicationDetail';
    angular.module('app').controller(controllerId, ['$scope', '$routeParams','$confirm', '$window', 'common', 'datacontext', applicationDetail]);

    function applicationDetail($scope, $routeParams,$confirm, $window, common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Application Detail';

        vm.applicationIdParameter = $routeParams.id;
        vm.isInEditMode = $routeParams.edit;
        vm.displayMessage = '';
        vm.detailMode = 'VIEW';
        vm.stages = [
            { label: 'None', value: 0 },
            { label: 'Favorabil', value: 1 },
            { label: 'Nefavorabil', value: 2 },
            { label: 'Partial', value: 3 },
            { label: 'Declinat', value: 4 },
            { label: 'Intern', value: 5 }
        ];
        vm.selectedStage = vm.stages[0];

        vm.application = {};

        vm.goBack = goBack;
        vm.saveApplication = saveApplication;

        activate();

        function activate() {
            var promises = [setupDetailView(), getApplication()];
            common.activateController(promises, controllerId)
                .then(function () {
//                    log('Activated Grave detail View');
                });
        }

        function getApplication() {
            if (vm.detailMode != 'ADD') {
                var applicationId = vm.applicationIdParameter;
                return datacontext.getApplicationById(applicationId).then(
                    function (data) {
                        vm.application = data;
                        datacontext.applications = data;
                        setStage();
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            }
        }

        function saveApplication() {
            if (vm.detailMode == 'ADD') {
                datacontext.addApplication(vm.application).then(
                    function(data){
                        logSuccess('Operatiunea a avut success!', null, true);
                        vm.goBack();
                    },
                    function(error){
                        logError('Va rugam incercati din nou.', null, true);
                    }
                );
            } else {
                vm.application.stage = vm.selectedStage.value;
                datacontext.editApplication(vm.application).then(
                    function(data){
                        logSuccess('Operatiunea a avut success!', null, true);
                        vm.goBack();
                    },
                    function(error){
                        logError('Va rugam incercati din nou.', null, true);
                    }
                );
            }

        }

        function setStage() {
            vm.stages.forEach(function(stage){
                if (stage.label == vm.application.stage){
                    vm.selectedStage = stage;
                    return;
                }
            });
        }

        function setupDetailView() {
            if (vm.applicationIdParameter == undefined) {
                // Add mode
                vm.displayMessage = "Adaugati o cerere";
                vm.detailMode = 'ADD';
            } else if (vm.isInEditMode == 'false') {
                // View mode
                vm.displayMessage = "Cerere";
                vm.detailMode = 'VIEW';
            } else {
                // Edit mode
                vm.displayMessage = "Editati cerere";
                vm.detailMode = 'EDIT';
            }
        }

        function goBack(){
            $window.history.back();
        }
    }
})();