(function () {
    'use strict';
    var controllerId = 'inhumations';
    angular.module('app').controller(controllerId, ['common', 'datacontext', '$scope', '$location', inhumations]);

    function inhumations(common, datacontext,$scope, $location) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Inhumations';

        vm.goToDeceasedDetail = goToDeceasedDetail;
        vm.goToBurialsDetail = goToBurialsDetail;

        vm.deceasedFilterFnc = deceasedFilterFnc;
        vm.deceasedEditFnc = deceasedEditFnc;
        vm.deceasedSelectedEntry = [];
        vm.deceasedFilterOptions = {filterText: ''};
        $scope.deceased = [];
        $scope.deceasedGridOptions = {
            data: 'deceased',
            selectedItems: vm.deceasedSelectedEntry,
            multiSelect: false,
            keepLastSelected: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.deceasedFilterOptions,
            columnDefs: [
                {field: 'ssn', displayName: 'CNP'},
                {field: 'lastName', displayName:'Nume'},
                {field: 'firstName', displayName:'Prenume'},
                {field: 'religion', displayName:'Religie'},
                {field: 'isAbandoned', displayName:'Abandonat'}
            ]
        };

        vm.burialsFilterFnc = burialsFilterFnc;
        vm.burialsAddFnc = burialsAddFnc;
        vm.burialsSelectedEntry = [];
        vm.burialsFilterOptions = {filterText: ''};
        $scope.burials = [];
        $scope.burialsGridOptions = {
            data: 'burials',
            selectedItems: vm.burialsSelectedEntry,
            multiSelect: false,
            keepLastSelected: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.burialsFilterOptions,
            columnDefs: [
                {field: 'cemeteryName', displayName: 'Nume cimitir'},
                {field: 'nrOfPermit', displayName: 'Numar adeverinta'},
                {field: 'plotNumber', displayName: 'Numar parcela'},
                {field: 'graveNumber', displayName: 'Mormant'},
                {field: 'date', displayName: 'Data'},
                {field: 'nrOfReceipt', displayName: 'Numar chitanta'},
                {field: 'deceased.ssn', displayName: 'CNP'},
                {field: 'deceased.lastName', displayName:'Nume'},
                {field: 'deceased.firstName', displayName:'Prenume'},
                {field: 'deceased.religion', displayName:'Religie'},
                {field: 'deceased.isAbandoned', displayName:'Abandonat'},
                {field: 'isMonument', displayName: 'Monument'},
                {field: 'additionalInfo', displayName: 'Detalii'}
            ]
        };


        activate();

        function activate() {
            var promises = [getDeceased(), getBurials()];
            common.activateController(promises, controllerId)
                .then(function () {});
        }

        function deceasedFilterFnc(filterText) {
            vm.deceasedFilterOptions.filterText = filterText;
        }

        function deceasedEditFnc() {
            if (vm.deceasedSelectedEntry.length > 0) {
                vm.goToDeceasedDetail(vm.deceasedSelectedEntry[0]);
            }
        }

        function burialsFilterFnc(filterText) {
            vm.burialsFilterOptions.filterText = filterText;
        }

        function burialsAddFnc() {
            vm.goToBurialsDetail();
        }

        function getDeceased() {
            return datacontext.getDeceased().then(
                function (response) {
                    // success
                    $scope.deceased = response;
                    datacontext.deceased = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function getBurials() {
            return datacontext.getBurials().then(
                function (response) {
                    // success
                    $scope.burials = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function goToDeceasedDetail(deceased){
            $location.path('/deceased/' + deceased.ssn + '/edit/' + true);
        }

        function goToBurialsDetail() {
                // Open in add mode
                $location.path('/inhumation' + '/edit/' + false);
        }
    }
})();