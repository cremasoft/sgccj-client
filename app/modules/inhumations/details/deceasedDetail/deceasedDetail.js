(function () {
    'use strict';
    var controllerId = 'deceasedDetail';
    angular.module('app').controller(controllerId, ['$scope', '$routeParams','$confirm', '$window', 'common', 'datacontext', deceasedDetail]);

    function deceasedDetail($scope, $routeParams,$confirm, $window, common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Deceased Detail';

        vm.deceasedIdParameter = $routeParams.id;
        vm.isInEditMode = $routeParams.edit;
        vm.displayMessage = '';
        vm.detailMode = 'VIEW';

        vm.deceased = {};

        vm.goBack = goBack;
        vm.saveDeceased = saveDeceased;

        activate();

        function activate() {
            var promises = [setupDetailView(), getDeceased()];
            common.activateController(promises, controllerId)
                .then(function () {
//                    log('Activated Grave detail View');
                });
        }

        function getDeceased() {
            if (vm.detailMode != 'ADD') {
                var deceasedId = vm.deceasedIdParameter;
                return datacontext.getDeceasedById(deceasedId).then(
                    function (data) {
                        vm.deceased = data;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            }
        }

        function saveDeceased() {
            datacontext.editDeceased(vm.deceased).then(
                function(data){
                    logSuccess('Operatiunea a avut success!', null, true);
                    vm.goBack();
                },
                function(error){
                    logError('Va rugam incercati din nou.', null, true);
                }
            );

        }

        function setupDetailView() {
            if (vm.isInEditMode == 'false') {
                // Add mode
                vm.displayMessage = "Adaugati un decedat";
                vm.detailMode = 'ADD';
            } else {
                // Edit mode
                vm.displayMessage = "Editati decedat";
                vm.detailMode = 'EDIT';
            }
        }

        function goBack(){
            $window.history.back();
        }
    }
})();