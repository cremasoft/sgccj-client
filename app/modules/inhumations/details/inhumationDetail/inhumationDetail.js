(function () {
    'use strict';
    var controllerId = 'inhumationDetail';
    angular.module('app').controller(controllerId, ['$scope', '$routeParams','$confirm', '$window', 'common', 'datacontext', inhumationDetail]);

    function inhumationDetail($scope, $routeParams,$confirm, $window, common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Inhumation Detail';

        vm.displayMessage = '';
        vm.detailMode = 'VIEW';

        vm.inhumation = {};

        vm.goBack = goBack;
        vm.saveBurial = saveBurial;

        vm.cemeteries = [];
        vm.selectedCemetery = {};

        activate();

        function activate() {
            var promises = [setupDetailView(), getCemeteries()];
            common.activateController(promises, controllerId)
                .then(function () {
//                    log('Activated Grave detail View');
                });
        }

        function getCemeteries() {
            datacontext.getCemeteries().then(
                function (data) {
                    parseCemeteries(data);
                },
                function (error) {
                    console.log(error);
                }
            );
        }

        function saveBurial() {
            vm.inhumation.cemeteryId = vm.selectedCemetery.value;
            datacontext.addBurial(vm.inhumation).then(
                function(data){
                    logSuccess('Operatiunea a avut success!', null, true);
                    vm.goBack();
                },
                function(error){
                    logError('Va rugam incercati din nou.', null, true);
                }
            );

        }

        function parseCemeteries(cemeteries){
            var index = 0;
            cemeteries.forEach(function(cemetery){
                vm.cemeteries[index++] = { label: cemetery.name, value: cemetery.id};
            })
        }

        function setupDetailView() {
            // Add mode
            vm.displayMessage = "Adaugati o inmormantare";
            vm.detailMode = 'ADD';
        }

        function goBack(){
            $window.history.back();
        }
    }
})();