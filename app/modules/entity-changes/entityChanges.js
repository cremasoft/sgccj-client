(function () {
    'use strict';
    var controllerId = 'entityChanges';
    angular.module('app').controller(controllerId, ['common', 'datacontext', '$scope','$filter', entityChanges]);

    function entityChanges(common, datacontext,$scope, $filter) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Entity Changes';


        vm.changesFilterFnc = changesFilterFnc;
        vm.changesFilterOptions = {filterText: ''};
        $scope.changes = [];
        $scope.changesGridOptions = {
            data: 'changes',
            enableRowSelection: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.changesFilterOptions,
            columnDefs: [
                {field: 'userName', displayName: 'Nume utilizator'},
                {field: 'date', displayName:'Data'},
                {field: 'detailInfo', displayName:'Detalii'},
                {field: 'documentNumber', displayName:'Numar document'}
            ]
        };


        activate();

        function activate() {
            var promises = [getEntityChanges()];
            common.activateController(promises, controllerId)
                .then(function () {});
        }

        function changesFilterFnc(filterText) {
            vm.changesFilterOptions.filterText = filterText;
        }

        function getEntityChanges() {
            return datacontext.getEntityChanges().then(
                function (response) {
                    // success
                    $scope.changes = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }
    }
})();