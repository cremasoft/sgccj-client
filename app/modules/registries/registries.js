﻿(function () {
    'use strict';
    var controllerId = 'registries';
    angular.module('app').controller(controllerId, ['common', 'datacontext', '$scope', registries]);

    function registries(common, datacontext, $scope) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Registries';

        vm.inhumationRegistryFilterFnc = inhumationRegistryFilterFnc;
        vm.inhumationRegistryExportFnc = inhumationRegistryExportFnc;
        vm.inhumationRegistrySelectedEntry = [];
        vm.inhumationRegistryFilterOptions = { filterText: '' };
        $scope.inhumationRegistryEntries = [];
        $scope.inhumationRegistryGridOptions = {
            data: 'inhumationRegistryEntries',
            selectedItems: vm.inhumationRegistrySelectedEntry,
            multiSelect: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.inhumationRegistryFilterOptions,
            columnDefs: [
                { field: 'lastName', displayName: 'Nume' },
                { field: 'firstName', displayName: 'Prenume' },
                { field: 'religion', displayName: 'Religie' },
                { field: 'cemeteryName', displayName: 'Nume cimitir' },
                { field: 'plotNumber', displayName: 'Numar parcela' },
                { field: 'date', displayName: 'Data inmormantare' }
            ]
        };

        vm.graveRegistryFilterFnc = graveRegistryFilterFnc;
        vm.graveRegistryExportFnc = graveRegistryExportFnc;
        vm.graveRegistrySelectedEntry = [];
        vm.graveRegistryFilterOptions = { filterText: '' };
        $scope.graveRegistryEntries = [];
        $scope.graveRegistryGridOptions = {
            data: 'graveRegistryEntries',
            selectedItems: vm.graveRegistrySelectedEntry,
            multiSelect: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.graveRegistryFilterOptions,
            columnDefs: [
                { field: 'cemeteryName', displayName: 'Nume cimitir' },
                { field: 'plotNumber', displayName: 'Numar parcela' },
                { field: 'graveNr', displayName: 'Numar mormant' },
                { field: 'burriedLastName', displayName: 'Nume decedat' },
                { field: 'burriedFirstName', displayName: 'Prenume decedat' },
                { field: 'inhumationDate', displayName: 'Data inmormantare' },
                { field: 'surface', displayName: 'Suprafata' },
                { field: 'notes', displayName: 'Observatii' },
                { field: 'firstNameOwner', displayName: 'Nume proprietar' },
                { field: 'lastNameOwner', displayName: 'Prenume proprietar' },
                { field: 'ownerAddress', displayName: 'Adresa proprietar' },
                { field: 'receiptNr', displayName: 'Numar chitanta' }
            ]
        };

        vm.graveMonumentsRegistryFilterFnc = graveMonumentsRegistryFilterFnc;
        vm.graveMonumentsRegistryExportFnc = graveMonumentsRegistryExportFnc;
        vm.graveMonumentsRegistrySelectedEntry = [];
        vm.graveMonumentsRegistryFilterOptions = { filterText: '' };
        $scope.graveMonumentsRegistryEntries = [];
        $scope.graveMonumentsRegistryGridOptions = {
            data: 'graveMonumentsRegistryEntries',
            selectedItems: vm.graveMonumentsRegistrySelectedEntry,
            multiSelect: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.graveMonumentsRegistryFilterOptions,
            columnDefs: [
                { field: 'cemeteryName', displayName: 'Nume cimitir' },
                { field: 'plotNumber', displayName: 'Numar parcela' },
                { field: 'graveNr', displayName: 'Numar mormant' },
                { field: 'burriedLastName', displayName: 'Nume decedat' },
                { field: 'burriedFirstName', displayName: 'Prenume decedat' },
                { field: 'inhumationDate', displayName: 'Data inmormantare' },
                { field: 'surface', displayName: 'Suprafata' },
                { field: 'notes', displayName: 'Observatii' },
                { field: 'firstNameOwner', displayName: 'Nume proprietar' },
                { field: 'lastNameOwner', displayName: 'Prenume proprietar' },
                { field: 'ownerAddress', displayName: 'Adresa proprietar' },
                { field: 'receiptNr', displayName: 'Numar chitanta' }
            ]
        };

        vm.deceasedRegistryFilterFnc = deceasedRegistryFilterFnc;
        vm.deceasedRegistryExportFnc = deceasedRegistryExportFnc;
        vm.deceasedRegistrySelectedEntry = [];
        vm.deceasedRegistryFilterOptions = { filterText: '' };
        $scope.deceasedRegistryEntries = [];
        $scope.deceasedRegistryGridOptions = {
            data: 'deceasedRegistryEntries',
            selectedItems: vm.deceasedRegistrySelectedEntry,
            multiSelect: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.deceasedRegistryFilterOptions,
            columnDefs: [
                { field: 'lastName', displayName: 'Nume' },
                { field: 'firstName', displayName: 'Prenume' },
                { field: 'cemeteryName', displayName: 'Nume cimitir' },
                { field: 'plotNumber', displayName: 'Numar parcela' },
                { field: 'graveNr', displayName: 'Numar mormanant' }
            ]
        };

        vm.abandonedDeceasedRegistryFilterFnc = abandonedDeceasedRegistryFilterFnc;
        vm.abandonedDeceasedRegistryExportFnc = abandonedDeceasedRegistryExportFnc;
        vm.abandonedDeceasedRegistrySelectedEntry = [];
        vm.abandonedDeceasedRegistryFilterOptions = { filterText: '' };
        $scope.abandonedDeceasedRegistryEntries = [];
        $scope.abandonedDeceasedRegistryGridOptions = {
            data: 'abandonedDeceasedRegistryEntries',
            selectedItems: vm.abandonedDeceasedRegistrySelectedEntry,
            multiSelect: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.abandonedDeceasedRegistryFilterOptions,
            columnDefs: [
                { field: 'cemeteryName', displayName: 'Nume cimitir' },
                { field: 'inhumationCertificateNumber', displayName: 'Adeverinta inhumare' },
                { field: 'plotNumber', displayName: 'Numar parcela' },
                { field: 'graveNr', displayName: 'Numar mormant' }
            ]
        };

        vm.inhumationRequestRegistryFilterFnc = inhumationRequestRegistryFilterFnc;
        vm.inhumationRequestRegistryExportFnc = inhumationRequestRegistryExportFnc;
        vm.inhumationRequestRegistrySelectedEntry = [];
        vm.inhumationRequestRegistryFilterOptions = { filterText: '' };
        $scope.inhumationRequestRegistryEntries = [];
        $scope.inhumationRequestRegistryGridOptions = {
            data: 'inhumationRequestRegistryEntries',
            selectedItems: vm.inhumationRequestRegistrySelectedEntry,
            multiSelect: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.inhumationRequestRegistryFilterOptions,
            columnDefs: [
                { field: 'date', displayName: 'Data inregistrarii' },
                { field: 'nrInfoCet', displayName: 'Nr. Infocet' },
                { field: 'stage', displayName: 'Stadiu de solutionare' }
            ]
        };

        vm.purchaseContractRegistryFilterFnc = purchaseContractRegistryFilterFnc;
        vm.purchaseContractRegistryExportFnc = purchaseContractRegistryExportFnc;
        vm.purchaseContractRegistrySelectedEntry = [];
        vm.purchaseContractRegistryFilterOptions = { filterText: '' };
        $scope.purchaseContractRegistryEntries = [];
        $scope.purchaseContractRegistryGridOptions = {
            data: 'purchaseContractRegistryEntries',
            selectedItems: vm.purchaseContractRegistrySelectedEntry,
            multiSelect: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.purchaseContractRegistryFilterOptions,
            columnDefs: [
                { field: 'contractNumber', displayName: 'Nr contract' },
                { field: 'dateIssued', displayName: 'Data eliberarii contractului' },
                { field: 'lastNameOwner', displayName: 'Nume' },
                { field: 'firstNameOwner', displayName: 'Prenume' },
                { field: 'addressOwner', displayName: 'Adresa' }
            ]
        };

        vm.intimationRegistryFilterFnc = intimationRegistryFilterFnc;
        vm.intimationRegistryExportFnc = intimationRegistryExportFnc;
        vm.intimationRegistrySelectedEntry = [];
        vm.intimationRegistryFilterOptions = { filterText: '' };
        $scope.intimationRegistryEntries = [];
        $scope.intimationRegistryGridOptions = {
            data: 'intimationRegistryEntries',
            selectedItems: vm.intimationRegistrySelectedEntry,
            multiSelect: false,
            enableColumnResize: true,
            plugins: [new ngGridFlexibleHeightPlugin()],
            filterOptions: vm.intimationRegistryFilterOptions,
            columnDefs: [
                { field: 'currentNr', displayName: 'Nr curent' },
                { field: 'lastName', displayName: 'Nume' },
                { field: 'firstName', displayName: 'Prenume' },
                { field: 'observations', displayName: 'Observatii' }
            ]
        };

        activate();

        function activate() {
            var promises = [getInhumationRegistry(), getGraveRegistry(), getGraveMonumentsRegistry(), getAbandonedDeceasedRegistry(),
                getDeceasedRegistry(), getInhumationRequestRegistry(), getPurchaseContractRegistry(), getIntimationRegistry()];
            common.activateController(promises, controllerId)
                .then(function () {});
        }

        function inhumationRegistryExportFnc() {
            datacontext.exportInhumationRegistry();
        }

        function inhumationRegistryFilterFnc(filterText) {
            vm.inhumationRegistryFilterOptions.filterText = filterText;
        }

        function graveRegistryExportFnc() {
            datacontext.exportGraveRegistry();
        }

        function graveRegistryFilterFnc(filterText) {
            vm.graveRegistryFilterOptions.filterText = filterText;
        }

        function graveMonumentsRegistryExportFnc() {
            datacontext.exportGraveMonumentsRegistry();
        }

        function graveMonumentsRegistryFilterFnc(filterText) {
            vm.graveMonumentsRegistryFilterOptions.filterText = filterText;
        }

        function abandonedDeceasedRegistryExportFnc() {
            datacontext.exportAbandonedDeceasedRegistry();
        }

        function abandonedDeceasedRegistryFilterFnc(filterText) {
            vm.abandonedDeceasedRegistryFilterOptions.filterText = filterText;
        }

        function deceasedRegistryExportFnc() {
            datacontext.exportDeceasedRegistry();
        }

        function deceasedRegistryFilterFnc(filterText) {
            vm.deceasedRegistryFilterOptions.filterText = filterText;
        }

        function inhumationRequestRegistryExportFnc() {
            datacontext.exportInhumationRequestRegistry();
        }

        function inhumationRequestRegistryFilterFnc(filterText) {
            vm.inhumationRequestRegistryFilterOptions.filterText = filterText;
        }

        function purchaseContractRegistryExportFnc() {
            datacontext.exportPurchaseContractRegistry();
        }

        function purchaseContractRegistryFilterFnc(filterText) {
            vm.purchaseContractRegistryFilterOptions.filterText = filterText;
        }

        function intimationRegistryExportFnc() {
            datacontext.exportIntimationRegistry();
        }

        function intimationRegistryFilterFnc(filterText) {
            vm.intimationRegistryFilterOptions.filterText = filterText;
        }

        function getInhumationRegistry() {
            datacontext.getInhumationRegistry().then(
                function (response) {
                    // success
                    $scope.inhumationRegistryEntries = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function getGraveRegistry() {
            datacontext.getGraveRegistry().then(
                function (response) {
                    // success
                    $scope.graveRegistryEntries = response;

                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function getGraveMonumentsRegistry() {
            datacontext.getGraveMonumentsRegistry().then(
                function (response) {
                    // success
                    $scope.graveMonumentsRegistryEntries = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function getAbandonedDeceasedRegistry() {
            datacontext.getAbandonedDeceasedRegistry().then(
                function (response) {
                    // success
                    $scope.abandonedDeceasedRegistryEntries = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function getDeceasedRegistry() {
            datacontext.getDeceasedRegistry().then(
                function (response) {
                    // success
                    $scope.deceasedRegistryEntries = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function getInhumationRequestRegistry() {
            datacontext.getInhumationRequestRegistry().then(
                function (response) {
                    // success
                    $scope.inhumationRequestRegistryEntries = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function getPurchaseContractRegistry() {
            datacontext.getPurchaseContractRegistry().then(
                function (response) {
                    // success
                    $scope.purchaseContractRegistryEntries = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }

        function getIntimationRegistry() {
            datacontext.getIntimationRegistry().then(
                function (response) {
                    // success
                    $scope.intimationRegistryEntries = response;
                },
                function (response) {
                    // error
                    alert("error");
                }
            );
        }
    }
})();